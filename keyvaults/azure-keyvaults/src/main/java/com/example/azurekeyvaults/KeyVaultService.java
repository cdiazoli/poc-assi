package com.example.azurekeyvaults;

import com.microsoft.azure.PagedList;
import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.models.SecretItem;

import java.util.ArrayList;
import java.util.List;

public class KeyVaultService {

    private KeyVaultClient keyVaultClient;
    private String vaultBaseUrl;

    public KeyVaultService(String clientId, String clientKey, String vaultBaseUrl) {
        this.keyVaultClient = new KeyVaultClient(new ClientSecretKeyVaultCredential(clientId, clientKey));
        this.vaultBaseUrl = vaultBaseUrl;
    }

    public String getKeyVaultByName(String keyVaultName) {
        String value = "NO_EXISTE";
        try {
            value = keyVaultClient.getSecret(vaultBaseUrl, keyVaultName).value();
        } catch (Exception ex) {
            System.out.println("keyvault no existe");
        }
        return value;

    }

    public List<KeyVaultDto> getKeyVaults() {
        List<KeyVaultDto> lstKeyVault = new ArrayList<>();
        try {
            PagedList<SecretItem> secrets = keyVaultClient.listSecrets(vaultBaseUrl);
            secrets.forEach(
                    secret -> {
                        String secretName = secret.identifier().name();
                        String value = getKeyVaultByName(secretName);
                        lstKeyVault.add(new KeyVaultDto(secretName, value));
                    }
            );

        } catch (Exception ex) {
            System.out.println("Error obteniendo keyvaults");
        }
        return lstKeyVault;

    }

}
