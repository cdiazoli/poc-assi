package com.example.azurekeyvaults;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.example")
public class AzureKeyvaultsService {

    public static void main(String[] args) {
        SpringApplication.run(AzureKeyvaultsService.class, args);
    }

}
