package com.example.azurekeyvaults;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/keyvaults"})
public class KeyVaultController {

    public KeyVaultController() {
    }

    @GetMapping
    public List<KeyVaultDto> getKeyVaults(@RequestParam String clientId,
                                          @RequestParam String clientKey,
                                          @RequestParam String vaultBaseUrl) {
        return new KeyVaultService(clientId, clientKey, vaultBaseUrl).getKeyVaults();
    }

    @PostMapping
    public List<KeyVaultDto> getKeyVaults(@RequestBody KeyVaultConfig keyVaultConfig) {
        return new KeyVaultService(keyVaultConfig.getClientId(), keyVaultConfig.getClientKey(), keyVaultConfig.getVaultBaseUrl()).getKeyVaults();
    }

    public static class KeyVaultConfig {

        private String clientId;
        private String clientKey;
        private String vaultBaseUrl;

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientKey() {
            return clientKey;
        }

        public void setClientKey(String clientKey) {
            this.clientKey = clientKey;
        }

        public String getVaultBaseUrl() {
            return vaultBaseUrl;
        }

        public void setVaultBaseUrl(String vaultBaseUrl) {
            this.vaultBaseUrl = vaultBaseUrl;
        }
    }
}
