package com.example.azurekeyvaults;

public class KeyVaultDto {

    private String application;
    private String fileName;
    private String propertyKey;
    private String propertyValue;
    private String keyVault;
    private String value;

    public KeyVaultDto(String application, String fileName, String propertyKey, String propertyValue, String keyVault) {
        this.application = application;
        this.fileName = fileName;
        this.propertyKey = propertyKey;
        this.propertyValue = propertyValue;
        this.keyVault = keyVault;
    }

    public KeyVaultDto(String keyVault, String value) {
        this.keyVault = keyVault;
        this.value = value;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public void setPropertyKey(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    public String getKeyVault() {
        return keyVault;
    }

    public void setKeyVault(String keyVault) {
        this.keyVault = keyVault;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
